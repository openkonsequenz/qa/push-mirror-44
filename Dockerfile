FROM openjdk:8-jre-alpine
COPY /target/contact-base-data.jar /usr/src/cbd/
WORKDIR usr/src/cbd
CMD ["java", "-jar", "-Dspring.config.name=application_localdev", "contact-base-data.jar"] 