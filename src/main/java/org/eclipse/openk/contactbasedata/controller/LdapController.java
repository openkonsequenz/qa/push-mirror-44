/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.service.LdapService;
import org.eclipse.openk.contactbasedata.viewmodel.LdapUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/ldap")
public class LdapController {

    @Autowired
    private LdapService ldapService;

    @ApiOperation(value = "Ermitteln der UserModules vom Auth'n'Auth-Service")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping( "/users")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    public List<LdapUser> getLdapUser() {
        return ldapService.getAllLdapUsers();
    }

    @ApiOperation(value = "Synchronisierung der User anhand des LDAPs")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping( "/sync")
    @Secured({"ROLE_KON-ADMIN"})
    public ResponseEntity<Object> syncLdapUser() {
        ldapService.synchronizeLDAP();
        return ResponseEntity.ok().build();
    }

}
