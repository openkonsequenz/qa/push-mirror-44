/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.ContactPersonService;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/contact-persons")
public class ContactPersonController {
    @Autowired
    private ContactPersonService contactPersonService;

    @GetMapping("/{contactUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen eines bestimmten Ansprechpartners zu einer Firma")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Ansprechpartner nicht gefunden."),
            @ApiResponse(code = 400, message = "Ungültige Anfrage."),
            @ApiResponse(code = 200, message = "Ansprechpartner erfolgreich gelesen.")})
    public ContactPersonDto findContactPerson(@PathVariable("contactUuid") UUID contactUuid){
        return contactPersonService.findContactPerson(contactUuid);
    }

    @PostMapping
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @ApiOperation(value = "Anlegen einer Kontaktperson")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Kontaktperson erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<ContactPersonDto> insertContactPerson(
            @Validated @RequestBody ContactPersonDto contactPersonDto) {
        ContactPersonDto savedContactPersonDto = contactPersonService.insertContactPerson(contactPersonDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedContactPersonDto.getContactUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedContactPersonDto);
    }

    @PutMapping("/{contactUuid}")
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @ApiOperation(value = "Ändern einer Kontaktperson")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Kontaktperson wurde aktualisiert"),
            @ApiResponse(code = 400, message = "Ungültige Eingabe"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public ResponseEntity updateContactPerson(@PathVariable UUID contactUuid, @Validated @RequestBody ContactPersonDto contactPersonDto) {

        if (!contactPersonDto.getContactUuid().equals(contactUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        contactPersonService.updateContactPerson(contactPersonDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{contactUuid}")
    @Secured({"ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Eine bestimmte Adresse eines bestimmten Kontakts löschen")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreich gelöscht"),
            @ApiResponse(code = 400, message = "Ungültige Anfrage"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public void deleteAddress(@PathVariable("contactUuid") UUID contactUuid) {

        contactPersonService.deleteContactPerson(contactUuid);
    }

}

