/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.contactbasedata.exceptions.BadRequestException;
import org.eclipse.openk.contactbasedata.service.CompanyService;
import org.eclipse.openk.contactbasedata.viewmodel.CompanyDto;
import org.eclipse.openk.contactbasedata.viewmodel.ContactPersonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @GetMapping("/{contactUuid}")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen einer Firma")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
                            @ApiResponse(code = 404, message = "Firma wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public CompanyDto getCompany(@PathVariable UUID contactUuid) {
        return companyService.findCompany(contactUuid);
    }

    @GetMapping
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen aller Firmen", notes = "Sortieren ist möglich. Firmen werden seitenweise geliefert.")
    @ResponseStatus(HttpStatus.OK)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreich durchgeführt")})
    public Page<CompanyDto> readCompanys(
            @RequestParam( "alsoShowAnonymous") Optional<Boolean> alsoShowAnonymous,
            @PageableDefault(sort = {"companyName"}, direction = Sort.Direction.ASC) Pageable pageable){
            return companyService.findCompanies(alsoShowAnonymous.orElse(false), pageable);
    }

    @PostMapping
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @ApiOperation(value = "Anlegen einer Firma")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Firma erfolgreich angelegt"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    public ResponseEntity<CompanyDto> insertCompany(
            @Validated @RequestBody CompanyDto companyDto) {
        CompanyDto savedCompanyDto = companyService.insertCompany(companyDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedCompanyDto.getContactUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedCompanyDto);
    }

    @PutMapping("/{contactUuid}")
    @Secured({"ROLE_KON-ADMIN", "ROLE_KON-WRITER"})
    @ApiOperation(value = "Ändern einer externen Person")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Firma wurde aktualisiert"),
            @ApiResponse(code = 400, message = "Ungültige Eingabe"),
            @ApiResponse(code = 404, message = "Nicht gefunden")})
    public ResponseEntity updateCompany(@PathVariable UUID contactUuid, @Validated @RequestBody CompanyDto companyDto) {

        if (!companyDto.getContactUuid().equals(contactUuid)) {
            throw new BadRequestException("invalid.uuid.path.object");
        }

        companyService.updateCompany(companyDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{contactUuid}/contact-persons")
    @Secured({"ROLE_KON-READER", "ROLE_KON-WRITER", "ROLE_KON-ADMIN"})
    @ApiOperation(value = "Anzeigen von Ansprechpartnern zu einer Firma")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erfolgreich durchgeführt"),
            @ApiResponse(code = 404, message = "Ansprechpartner wurden nicht gefunden")})
    public List<ContactPersonDto> findContactPersons(
            @PathVariable("contactUuid") UUID contactUuid,
            @RequestParam("alsoShowAnonymous") Optional<Boolean> alsoShowAnonymous ) {

        return companyService.findContactPersonsToCompany(
                contactUuid, alsoShowAnonymous.orElse(false));
    }

}

