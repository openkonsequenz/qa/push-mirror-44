/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.model;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
public class TblExternalPerson {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "tbl_external_person_id_seq")
    @SequenceGenerator(name = "tbl_external_person_id_seq", sequenceName = "tbl_external_person_id_seq", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;
    private String firstName;
    private String lastName;
    private String title;

    @OneToOne
    @JoinColumn( name = "fk_contact_id")
    private TblContact contact;

    @ManyToOne
    @JoinColumn( name = "fk_salutation_id")
    private RefSalutation salutation;

    @ManyToOne
    @JoinColumn( name = "fk_ref_person_type_id")
    private RefPersonType refPersonType;



}