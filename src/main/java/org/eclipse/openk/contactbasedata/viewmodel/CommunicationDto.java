/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.viewmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Data
public class CommunicationDto implements Serializable {

    @JsonProperty("id")
    private UUID  uuid;
    private String communicationData;
    private String note;

    @JsonProperty("contactId")
    private UUID contactUuid;

    //fromCommunicationType
    @JsonProperty("communicationTypeId")
    @ApiModelProperty(required = true)
    @NotNull(message = "Field 'communicationTypeUuid' is mandatory")
    private UUID communicationTypeUuid;

    private String communicationTypeType;

    private String communicationTypeDescription;
}

