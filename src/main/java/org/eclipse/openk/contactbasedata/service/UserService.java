/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.api.AuthNAuthApi;
import org.eclipse.openk.contactbasedata.model.KeyCloakUser;
import org.eclipse.openk.contactbasedata.viewmodel.UserModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private AuthNAuthApi authNAuthApi;

    public List<UserModule> getUserModules() {
        String bearerToken = (String)SecurityContextHolder.getContext().getAuthentication().getDetails();
        return authNAuthApi.getUserModulesForUser(bearerToken);
    }

    public List<KeyCloakUser> getKeycloakUsers() {
        String bearerToken = (String)SecurityContextHolder.getContext().getAuthentication().getDetails();
        return authNAuthApi.getKeycloakUsers(bearerToken);
    }
}
