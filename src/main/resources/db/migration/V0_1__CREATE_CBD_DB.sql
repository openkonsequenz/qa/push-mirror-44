﻿-----------------------------------------------------------------------------------
-- *******************************************************************************
-- * Copyright (c) 2019 Contributors to the Eclipse Foundation
-- *
-- * See the NOTICE file(s) distributed with this work for additional
-- * information regarding copyright ownership.
-- *
-- * This program and the accompanying materials are made available under the
-- * terms of the Eclipse Public License v. 2.0 which is available at
-- * http://www.eclipse.org/legal/epl-2.0.
-- *
-- * SPDX-License-Identifier: EPL-2.0
-- *******************************************************************************
-----------------------------------------------------------------------------------
-- CREATE ROLE CBD_SERVICE LOGIN
-- NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
-- ALTER ROLE CBD_SERVICE with password 'cbd_service';

CREATE TABLE public.VERSION
(
  ID integer NOT NULL,
  VERSION character varying(50) NOT NULL,
  CONSTRAINT REF_VERSION_PKEY PRIMARY KEY (id)
);

ALTER TABLE public.VERSION
  OWNER TO CBD_SERVICE;
GRANT ALL ON TABLE public.VERSION TO CBD_SERVICE;

INSERT INTO public.VERSION (ID, VERSION) VALUES ( 1, '00-DEV' );




