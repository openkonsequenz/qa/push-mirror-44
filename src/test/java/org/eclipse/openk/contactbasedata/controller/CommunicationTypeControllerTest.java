/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.CommunicationTypeService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationTypeDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;




@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test") // Todo: Find a better way to configure the tests
public class CommunicationTypeControllerTest {

    @MockBean
    private CommunicationTypeService communicationTypeService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnAllCommunicationTypes() throws Exception {
        List<CommunicationTypeDto> ctDtos = MockDataHelper.mockCommunicationTypeDtoList();

        when(communicationTypeService.findCommunicationTypes()).thenReturn(ctDtos);

        mockMvc.perform(get("/communication-types"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldReturnSingleCommunicationType() throws Exception {
        CommunicationTypeDto communicationTypeDto = MockDataHelper.mockCommunicationTypeDto();

        when(communicationTypeService.findCommunicationType(any(UUID.class))).thenReturn(communicationTypeDto);

        mockMvc.perform(get("/communication-types/37454f86-2006-11ea-978f-2e728ce88125"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldInsertCommunicationType() throws Exception {
        CommunicationTypeDto communicationTypeDto = MockDataHelper.mockCommunicationTypeDto();
        communicationTypeDto.setType("Trommel");
        communicationTypeDto.setDescription("Djembe");

        when(communicationTypeService.insertCommunicationType(any(CommunicationTypeDto.class)))
                .thenReturn(communicationTypeDto);

        mockMvc.perform(post("/communication-types")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(communicationTypeDto)))
                .andExpect(jsonPath("$.type", Matchers.is("Trommel")))
                .andExpect(jsonPath("$.description", Matchers.is("Djembe" )));
    }

    @Test
    public void shouldUpdateCommunicationType() throws Exception {
        CommunicationTypeDto dto = MockDataHelper.mockCommunicationTypeDto();

        when( communicationTypeService.updateCommunicationType(any(CommunicationTypeDto.class))).thenReturn(dto);

        mockMvc.perform(put("/communication-types/{uuid}", dto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(dto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldNotUpdateCommunicationTypeDueToError() throws Exception {
        CommunicationTypeDto communicationTypeDto = MockDataHelper.mockCommunicationTypeDto();
        communicationTypeDto.setUuid(UUID.randomUUID());

        when(  communicationTypeService.updateCommunicationType(any(CommunicationTypeDto.class))).thenReturn(communicationTypeDto);

        // use different UUIDs for path and object
        mockMvc.perform(put("/communication-types/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(communicationTypeDto)))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void shouldDeleteCommunicationType() throws Exception {
        UUID uuid = UUID.fromString("05ff2344-20a5-11ea-978f-2e728ce88125");
        doNothing().when(communicationTypeService).removeCommunicationType(isA(UUID.class));
        mockMvc.perform(delete("/communication-types/"+uuid.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
        verify(communicationTypeService, times( 1 )).removeCommunicationType(uuid);
    }

    @Test
    public void shouldFailWithDeleteCommunicationType() throws Exception {
        UUID uuid = UUID.fromString("05ff2344-20a5-11ea-978f-2e728ce88125");
        doThrow(new RuntimeException("Base Exception")).when(communicationTypeService).removeCommunicationType(isA(UUID.class));
        mockMvc.perform(delete("/communication-types/"+uuid.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.SC_CONFLICT));
    }

}