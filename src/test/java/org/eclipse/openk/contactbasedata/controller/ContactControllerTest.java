/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.model.VwDetailedContact;
import org.eclipse.openk.contactbasedata.service.ContactService;
import org.eclipse.openk.contactbasedata.service.util.SearchContactsFilterParams;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class ContactControllerTest {

    @MockBean
    private ContactService contactService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    void shouldReturnAllContacts() throws Exception {
        Page<VwDetailedContact> retPage = MockDataHelper.mockVDetailedContactPage();

        when(contactService.findDetailedContacts(any(SearchContactsFilterParams.class), any(Pageable.class))).thenReturn(retPage);

        mockMvc.perform(get("/contacts"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("totalElements", is(2)));
    }

    @Test
    void shouldReturnSingleContactByUuid() throws Exception {
        VwDetailedContact vwDetailedContact = MockDataHelper.mockVDetailedContact();

        when(contactService.findDetailedContactByUuid(any(UUID.class))).thenReturn(vwDetailedContact);

        mockMvc.perform(get("/contacts/{uuid}", UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}