/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.PersonTypeService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.PersonTypeDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class PersonTypeControllerTest {

    @MockBean
    private PersonTypeService personTypeService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnAllPersonTypes() throws Exception {
        List<PersonTypeDto> sds = MockDataHelper.mockPersonTypesDtos();

        when(personTypeService.findAllPersonTypes()).thenReturn(sds);

        mockMvc.perform(get("/personTypes"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldReturnSinglePersonType() throws Exception {
        PersonTypeDto personTypeDto = MockDataHelper.mockPersonTypeDto();

        when(personTypeService.getPersonTypeByUuid(any(UUID.class))).thenReturn(personTypeDto);

        mockMvc.perform(get("/personTypes/37454f86-2006-11ea-978f-2e728ce88125"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void shouldInsertPersonType() throws Exception {
        PersonTypeDto personTypeDto = MockDataHelper.mockPersonTypeDto();
        personTypeDto.setType("Master");
        personTypeDto.setDescription("of the universe");

        when(personTypeService.insertPersonType(any(PersonTypeDto.class)))
                .thenReturn(personTypeDto);

        mockMvc.perform(post("/personTypes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(personTypeDto)))
                .andExpect(jsonPath("$.type", Matchers.is("Master")))
                .andExpect(jsonPath("$.description", Matchers.is("of the universe" )));
    }

    @Test
    void shouldUpdatePersonType() throws Exception {
        PersonTypeDto personTypeDto = MockDataHelper.mockPersonTypeDto();
        personTypeDto.setUuid(UUID.randomUUID());

        when(  personTypeService.updatePersonType(any(PersonTypeDto.class))).thenReturn(personTypeDto);

        mockMvc.perform(put("/personTypes/{uuid}", personTypeDto.getUuid().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(personTypeDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdatePersonTypeDueToError() throws Exception {
        PersonTypeDto personTypeDto = MockDataHelper.mockPersonTypeDto();
        personTypeDto.setUuid(UUID.randomUUID());

        when(  personTypeService.updatePersonType(any(PersonTypeDto.class))).thenReturn(personTypeDto);

        // use different UUIDs for path and object
        mockMvc.perform(put("/personTypes/{uuid}", UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(personTypeDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldDeletePersonType() throws Exception {
        mockMvc.perform(delete("/personTypes/05ff2344-20a5-11ea-978f-2e728ce88125")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

}