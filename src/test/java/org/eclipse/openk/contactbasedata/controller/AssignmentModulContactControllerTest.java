/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.contactbasedata.ContactBaseDataApplication;
import org.eclipse.openk.contactbasedata.service.AssignmentModulContactService;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.AssignmentModulContactDto;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = ContactBaseDataApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AssignmentModulContactControllerTest {

    @MockBean
    private AssignmentModulContactService assignmentService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldReturnAssignmentsForAContact() throws Exception {
        List<AssignmentModulContactDto> listDtos = MockDataHelper.mockAssignmentModulContactDtoList();

        when(assignmentService.getAssignments(any(UUID.class))).thenReturn(listDtos);

        mockMvc.perform(get("/contacts/{uuid}/assignments", UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    void shouldReturnAssignmentForAContact() throws Exception {
        AssignmentModulContactDto dto = MockDataHelper.mockAssignmentModulContactDto();

        when(assignmentService.getAssignment(any(UUID.class), any(UUID.class))).thenReturn(dto);

        mockMvc.perform(get("/contacts/{uuid}/assignments/{uuid}", UUID.randomUUID(), UUID.randomUUID()))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    void shouldInsertAssignment() throws Exception {
        AssignmentModulContactDto assignmentDto = MockDataHelper.mockAssignmentModulContactDto();
        assignmentDto.setUuid(UUID.fromString("394dce82-41af-11ea-b77f-2e728ce88125"));
        assignmentDto.setContactUuid(UUID.fromString("5b164ab2-41af-11ea-b77f-2e728ce88125"));
        assignmentDto.setAssignmentDate(Date.from(Instant.parse("2020-12-23T23:00:00Z")));
        assignmentDto.setAssignmentNote("nur für Tests");
        assignmentDto.setDeletionLockUntil(Date.from(Instant.parse("2021-12-23T23:00:00Z")));
        assignmentDto.setExpiringDate(Date.from(Instant.parse("2022-12-23T23:00:00Z")));
        assignmentDto.setModulName("Störinfos");

        when(assignmentService.insertAssignment(any(UUID.class), any(AssignmentModulContactDto.class)))
                .thenReturn(assignmentDto);

        mockMvc.perform(post("/contacts/{uuid}/assignments", assignmentDto.getContactUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(assignmentDto)))
                .andExpect(jsonPath("$.id", Matchers.is("394dce82-41af-11ea-b77f-2e728ce88125")))
                .andExpect(jsonPath("$.contactId", Matchers.is("5b164ab2-41af-11ea-b77f-2e728ce88125" )))
                .andExpect(jsonPath("$.assignmentDate", Matchers.is("2020-12-23T23:00:00.000Z" )))
                .andExpect(jsonPath("$.assignmentNote", Matchers.is("nur für Tests" )))
                .andExpect(jsonPath("$.deletionLockUntil", Matchers.is("2021-12-23T23:00:00.000Z" )))
                .andExpect(jsonPath("$.expiringDate", Matchers.is("2022-12-23T23:00:00.000Z" )))
                .andExpect(jsonPath("$.modulName", Matchers.is("Störinfos" )));

    }


    @Test
    void shouldUpdateAssignment() throws Exception {

        AssignmentModulContactDto assignmentDto = MockDataHelper.mockAssignmentModulContactDto();
        when( assignmentService.updateAssignment(any(UUID.class), any(AssignmentModulContactDto.class))).thenReturn(assignmentDto);

        mockMvc.perform(put("/contacts/{contactUuid}/assignments/{assignmentUuid}", assignmentDto.getContactUuid(), assignmentDto.getUuid())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(assignmentDto)))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void shouldNotUpdateAddress() throws Exception {

        AssignmentModulContactDto assignmentDto = MockDataHelper.mockAssignmentModulContactDto();
        when( assignmentService.updateAssignment(any(UUID.class), any(AssignmentModulContactDto.class))).thenReturn(assignmentDto);

        mockMvc.perform(put("/contacts/{contactUuid}/assignments/{assignmentUuid}", assignmentDto.getContactUuid(), UUID.randomUUID().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(assignmentDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldDeleteAddress() throws Exception {
        mockMvc.perform(delete("/contacts/{contactUuid}/assignments/{assignmentUuid}", UUID.randomUUID(), UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}