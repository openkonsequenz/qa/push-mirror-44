/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.contactbasedata.service;

import org.eclipse.openk.contactbasedata.config.TestConfiguration;
import org.eclipse.openk.contactbasedata.model.RefCommunicationType;
import org.eclipse.openk.contactbasedata.repository.CommunicationTypeRepository;
import org.eclipse.openk.contactbasedata.support.MockDataHelper;
import org.eclipse.openk.contactbasedata.viewmodel.CommunicationTypeDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
class CommunicationTypeServiceTest {
    @Qualifier("myCommunicationTypeService")
    @Autowired
    private CommunicationTypeService communicationTypeService;

    @MockBean
    private CommunicationTypeRepository communicationTypeRepository;

    @Test
    void shouldFindAllCommunicationTypes() {
        List<RefCommunicationType> rct = MockDataHelper.mockRefCommunicationTypeList();
        when(communicationTypeRepository.findAll()).thenReturn(rct);
        List<CommunicationTypeDto> retVals = communicationTypeService.findCommunicationTypes();

        assertEquals(rct.size(), retVals.size());
        assertEquals(3, rct.size());
        assertEquals(rct.get(1).getUuid(), retVals.get(1).getUuid());
    }


    @Test
    void shouldReturnCommunicationTypeByUuid() {

        RefCommunicationType rct = MockDataHelper.mockRefCommunicationType();
        rct.setType("Testtyp");
        when( communicationTypeRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(rct));

        CommunicationTypeDto communicationTypeDto = communicationTypeService.findCommunicationType(UUID.randomUUID());
        assertEquals( communicationTypeDto.getUuid(), rct.getUuid() );
        assertEquals( communicationTypeDto.getType(), rct.getType() );
    }

    @Test
    void shouldInsertCommunicationType(){
        RefCommunicationType rfc = MockDataHelper.mockRefCommunicationType();

        rfc.setUuid(UUID.fromString("b1a9b6de-3129-11ea-978f-2e728ce88125"));
        rfc.setType("Brieftaube");
        rfc.setDescription("Ringeltaube");

        CommunicationTypeDto communicationTypeDto = MockDataHelper.mockCommunicationTypeDto();
        communicationTypeDto.setUuid(null);

        when( communicationTypeRepository.save( any( RefCommunicationType.class) )).thenReturn(rfc);

        CommunicationTypeDto savedCommunicationTypeDto = communicationTypeService.insertCommunicationType(communicationTypeDto);
        assertEquals("Brieftaube", savedCommunicationTypeDto.getType());
        assertNotNull( savedCommunicationTypeDto.getUuid());
    }

    @Test
    void shouldUpdateCommunicationType() {

        RefCommunicationType rfc = MockDataHelper.mockRefCommunicationType();
        rfc.setType("Rauchzeichen");

        CommunicationTypeDto communicationTypeDto = MockDataHelper.mockCommunicationTypeDto();
        communicationTypeDto.setType("Leuchtfeuer");

        when( communicationTypeRepository.findByUuid( any(UUID.class)) ).thenReturn(Optional.of(rfc));
        when( communicationTypeRepository.save( any(RefCommunicationType.class)) ).thenReturn(rfc);
        CommunicationTypeDto communicationTypeDtoUpdated = communicationTypeService.updateCommunicationType(communicationTypeDto);

        assertEquals(rfc.getUuid(), communicationTypeDtoUpdated.getUuid());
        assertEquals("Rauchzeichen", communicationTypeDtoUpdated.getType());
    }


    @Test
    void shouldDeleteCommunicationType() {

        RefCommunicationType rft = MockDataHelper.mockRefCommunicationType();

        when(communicationTypeRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(rft));
        Mockito.doNothing().when(communicationTypeRepository).delete( isA( RefCommunicationType.class ));
        communicationTypeService.removeCommunicationType(rft.getUuid());

        Mockito.verify(communicationTypeRepository, times(1)).delete( rft );
    }
}
